import cv2
import numpy as np
from collections import Counter
import json


result = []
with open("body_rectangle.txt", "r") as filestream:
    with open("split.txt", "r") as filestreamtwo:
        with open("cbody.txt", "w") as filestreamthree:
            for line in filestream:
                print line
                currentline = line.split(" ")
                image = cv2.imread(currentline[0])
                print currentline[0]
                for x in range(int(currentline[1]), int(currentline[3])-int(currentline[1])):
                	for y in range(int(currentline[2]), int(currentline[4])-int(currentline[2])):
                		result.append(list(image[x,y]))
            count = Counter(map(tuple, result))
            filestreamthree.write(json.dumps(str(count)))