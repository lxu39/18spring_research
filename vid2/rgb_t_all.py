import cv2
import numpy as np
from collections import Counter
import glob as gb
import json

'''result = []
image = cv2.imread("frame1.jpg",1)
height, width = image.shape[:2]
for x in range(height):
	for y in range(width):
		result.append(list(image[x,y]))

count = Counter(map(tuple, result))
print len(result)
print len(count)
print count'''


number = 0
img_path = gb.glob("frame*.jpg")
for path in img_path:
	result = []
	print path
	number += 1
	print number
	image = cv2.imread(path)
	height, width = image.shape[:2]
	for x in range(height):
		for y in range(width):
			result.append(list(image[x,y]))
	count = Counter(map(tuple, result))
	filename = 'rgb_single_t_' + path.split('.')[0] + '.txt'
	with open(filename, 'w') as file:
		file.write(json.dumps(str(count)))
