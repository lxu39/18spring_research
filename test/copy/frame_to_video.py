# encoding: UTF-8
import glob as gb
import cv2

import re
re_digits = re.compile(r'(\d+)')
def embedded_numbers(s):
    pieces = re_digits.split(s)             # split into digits/nondigits
    pieces[1::2] = map(int, pieces[1::2])   # turn digits into numbers
    return pieces
def sort_strings_with_embedded_numbers(alist):
    aux = [ (embedded_numbers(s), s) for s in alist ]
    aux.sort( )
    return [ s for __, s in aux ]           # convention: _ _ means "ignore"

img_path = sorted(gb.glob('*.jpg'),key=embedded_numbers)
print img_path
'''new_path = []
for item in img_path:
	edit_path = item.lstrip("frame").rstrip(".jpg")
	new_path.append(edit_path)'''
#videoWriter = cv2.VideoWriter('test.mp4', cv2.VideoWriter_fourcc(*'MJPG'), 25, (960,540))
videoWriter = cv2.VideoWriter('test.avi', cv2.VideoWriter_fourcc(*'XVID'), 15, (960,540))
for path in img_path:
	#print path
	img  = cv2.imread(path)
	img = cv2.resize(img,(960,540))
	videoWriter.write(img)