import cv2
import numpy as np
import glob as gb
from PIL import Image

rgb_list = []

with open("newfile.txt", "r") as filestream:
    for line in filestream:
        currentline = line.split("):")[0]
        tmp = currentline.split(",")
        rgb_tuple = (int(tmp[0]), int(tmp[1]), int(tmp[2]))
        rgb_list.append(rgb_tuple)

img_path = gb.glob("0.jpg")
for path in img_path:
	img = Image.open(path)
	im = img.load()
	width, height = img.size
	for x in range(height):
		for y in range(width):
			if im[y,x] in rgb_list:
				print y,x
				im[y,x] = (255,255,255)
	print "saving"
	im.save(path)