# -*- coding: utf8 -*-

import cv2
import numpy as np
import glob as gb

'''image = cv2.imread("frame1.jpg",1)
height, width = image.shape[:2]
#print height, width
for x in range(height):
	for y in range(width):
		temp = str(x) + " " + str(y) + " " + str(image[x,y])
		with open("data.txt", "a") as f:
			f.write(temp+"\n")
			temp = ""
'''

img_path = gb.glob("frame*.jpg")
for path in img_path:
	print path
	image = cv2.imread(path)
	height, width = image.shape[:2]
	#print height, width
	frame_num = str(path).split('.')[0]
	for x in range(height):
		for y in range(width):
			temp = str(x) + " " + str(y) + " " + str(frame_num) + " ," + str(image[x,y])
			with open("data.txt", "a") as f:
				f.write(temp+"\n")
				temp = ""